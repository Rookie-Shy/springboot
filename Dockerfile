FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/spring-boot-docker-demo-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 7001
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]